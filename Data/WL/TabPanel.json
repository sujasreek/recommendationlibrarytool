[
    {
        "ID": "143a",
        "Scenario": "Color contrast for regular text or link or image of text",
        "CheckpointName": "1.4.3.a Color Contrast (regular text)",
        "AssureCkpointID": "27",
        "AssureDetailsID": "Text content lacks 4.5 to 1 contrast ratio",
        "IssueDescription": "Color contrast ratio on the tab elements is less than 4.5:1 with the background.",
        "Recommendation": "People who have low vision or are colorblind, could encounter some difficulty distinguishing text from its background if the contrast is insufficient. There are nearly three times more individuals with low vision than those with total blindness; and one out of twelve people has some sort of color deficiency. So, it is critical to consider adequate contrast between text and backgrounds.\\nRegular sized text must have a minimum contrast ratio of 4.5:1 against its background.\\n\\nFix this issue by adjusting the text and / or background to increase the contrast to at least 4.5 to 1."
    },
    {
        "ID": "211a",
        "Scenario": "Keyboard operability missing for the elements",
        "CheckpointName": "2.1.1.a Keyboard Navigation",
        "AssureCkpointID": "31",
        "AssureDetailsID": "Action is not accessible by keyboard alone",
        "IssueDescription": "Tabbed elements not keyboard operable.",
        "Recommendation": "All functionality provided for interactive elements with the mouse must also be operable through keyboard and touch interfaces.\\n\\nFix this issue by ensuring the component can be used by the keyboard.View the full ARIA design pattern including expected keyboard interaction and examples: https://www.w3.org/TR/wai-aria-practices-1.1/#tabpanel"
    },
    {
        "ID": "212a",
        "Scenario": "Keyboard trap at an element",
        "CheckpointName": "2.1.2.a Keyboard Trap",
        "AssureCkpointID": "33",
        "AssureDetailsID": "Keyboard trap",
        "IssueDescription": "Keyboard trap exists within one tabbed element and its content.",
        "Recommendation": "It is critical to ensure that web content does not \"trap\" the keyboard focus for users who rely on keyboard functionality. Keyboard traps can occur when there are items like custom widgets, embedded applications, and other content formats all on one page. If there is a keyboard trap on a web page, sometimes the only way to escape it involves either using a mouse (which just is not an option for some keyboard users) or refreshing the web page. Keyboard traps can make interacting with web content extremely difficult for keyboard users."
    },
    {
        "ID": "243a",
        "Scenario": "Incorrect focus order",
        "CheckpointName": "2.4.3.a Focus Order",
        "AssureCkpointID": "41",
        "AssureDetailsID": "Tab order is not logical",
        "IssueDescription": "Tab order is not appropriate. Tabbing navigates from one element to another illogically.",
        "Recommendation": "If a keyboard user—whether sighted or blind—uses the tab key to go through all of the focusable elements (links, buttons, form elements, etc.), the browser will start with the focusable elements at the top and proceed linearly through all the focusable elements until reaching the bottom.\\nWhen keyboard users tab through the focusable items (buttons, links, form elements, custom controls, etc.), the order needs to make sense, so they don't get confused.\\n\\nFix the issue by ensuring the DOM order is logical so that the focus order will naturally follow. Do not use positive tabindex values to \"correct\" the tab order as it will not change the reading order for screen reader users and will almost always cause unintended, significant problems."
    },
    {
        "ID": "247a",
        "Scenario": "Missing focus indicator",
        "CheckpointName": "2.4.7.a Focus Visible",
        "AssureCkpointID": "46",
        "AssureDetailsID": "Focus indicator is not clearly visible",
        "IssueDescription": "Visible focus indicator is not available on the elements while tabbing through them.",
        "Recommendation": "When a person interacts with a webpage via a keyboard, they must be able to clearly see where the focus is. A keyboard focus indicator can take different forms; it does not have to be a boring box. But the goal is to provide a difference in contrast between a component's default and focused states of at least 3 to 1.\\n\\nCommon focus indicator solutions include:\\n1. A 2px box around the focused element\\n2. A change in the background color of the focused element\\n3. The addition of an icon, such as an arrow next to a menu item\\n4. The addition of a thick line under or next to a the focus element\\n5. A change to the text of the component such as making it bold and/or underlined"
    },
    {
        "ID": "412b",
        "Scenario": "Tab markup missing for the elements that appear as such.",
        "CheckpointName": "4.1.2.b Custom controls",
        "AssureCkpointID": "64",
        "AssureDetailsID": "NA",
        "IssueDescription": "Element functions as tab but is not defined as such.",
        "Recommendation": "The element looks and acts like a tab panel but the necessary ARIA markup is missing to convey the necessary information to screen reader users.\\n\\nFix this problem by using the following roles and attributes:\\n1. The element that serves as the container for the set of tabs has role tablist.\\n2. Each element that serves as a tab has role tab and is contained within the element with role tablist.\\n3. Each element that contains the content panel for a tab has role tabpanel.\\n4. Each element with role tab has the property aria-controls referring to its associated tabpanel element.\\n5. The active tab element has the state aria-selected set to true and all other tab elements have it set to false.\\n6. Each element with role tabpanel has the property aria-labelledby referring to its associated tab element.\\n7. If the tablist element is vertically oriented, it has the property aria-orientation set to vertical. The default value of aria-orientation for a tablist element is horizontal.\\n8. Ensure the recommended keyboard pattern is also implemented\\nView the full ARIA design pattern including expected keyboard interaction and examples: https://www.w3.org/TR/wai-aria-practices-1.1/#tabpanel"
    }
]