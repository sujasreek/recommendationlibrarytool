[
    {
        "ID": "211a",
        "Scenario": "Keyboard operability missing for the elements",
        "CheckpointName": "2.1.1.a Keyboard Navigation",
        "AssureCkpointID": "2.1.1.a Keyboard Navigation",
        "AssureDetailsID": "Action is not accessible by keyboard alone",
        "IssueDescription": "Tabbed elements and the list items under them are not keyboard operable.",
        "Recommendation": "RULE:\\nFunctionality MUST be available using the keyboard, unless the functionality cannot be accomplished in any known way using a keyboard.\\n\\nHOW TO FIX:\\nFix this issue by ensuring the component can be used by the keyboard.\\n\\nREFERENCE:\\nDeque University:https://dequeuniversity.com/class/input-methods2/keyboard-input/functionality\\n\\nBACKGROUND:\\nSome people cannot use a mouse due to vision or motor disabilities. Content that can be operated with a mouse must also be made operable with keyboard. When content is operable through a keyboard, it becomes operable by a variety assistive technologies such as speech input software, sip-and-puff software, on-screen keyboards, scanning software, and alternate keyboards."
    },
    {
        "ID": "243a",
        "Scenario": "Incorrect focus order",
        "CheckpointName": "2.4.3.a Focus Order",
        "AssureCkpointID": "2.4.3.a Focus Order",
        "AssureDetailsID": "Tab order is not logical",
        "IssueDescription": "Tab order is not appropriate, it does not move from one tabbed element to another in sequential order.",
        "Recommendation": "RULE:\\nThe reading and navigation order MUST be logical and intuitive.\\n\\nHOW TO FIX:\\nFix this issue by ensuring the DOM order is logical so that the focus order will naturally follow. Do not use positive tabindex values to \"correct\" the tab order as it will not change the reading order for screen reader users and will almost always cause unintended, significant problems.\\n\\nREFERENCE:\\nDeque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/tab-order\\n\\nBACKGROUND:\\nWhen keyboard focusable components do not receive focus in a logical order, people with mobility impairments, reading disabilities, and low vision are all impacted. The keyboard focus order must be logical and consistent with the layout of the content. (Note: This does not mean that the focus order has to be identical to the visual order, as long as the user can still understand and operate the content.) A logical focus order makes interaction with content predictable for people who rely on a keyboard to interact with a web content."
    },
    {
        "ID": "247a",
        "Scenario": "Missing focus indicator",
        "CheckpointName": "2.4.7.a Focus Visible",
        "AssureCkpointID": "2.4.7.a Focus Visible",
        "AssureDetailsID": "Focus indicator is not clearly visible",
        "IssueDescription": "There is no visible focus indicator available on each tab in tabbed elements.",
        "Recommendation": "RULE :\\nAll focusable elements MUST have a visual focus indicator when in focus.\\nFocusable elements SHOULD have enhanced visual focus indicator styles.\\n\\nHOW TO FIX:\\nCommon keyboard focus indicator solutions include:\\n1.A 2px box around the focused element\\n2.A change in the background color of the focused element\\n3.The addition of an icon, such as an arrow, next to a menu item\\n4.The addition of a thick line under or next to the focused element\\n5.A change to the text of the component such as making it bold and/or underlined\\nA keyboard focus indicator can take different forms; it does not have to be a boring box. But the goal is to provide a difference in contrast between a component's default and focused states of at least 3 to 1.\\n\\nREFERENCE:\\nDeque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/visual-focus-indicator\\n\\nBACKGROUND:\\nWhen a visible keyboard focus indicator is not provided, sighted keyboard users will have no idea which link or control has focus making it extremely difficult, if not impossible, to interact with the content. Browsers provide default focus indicators for natively focusable elements, but these may be very difficult to see depending on the color of the control and the page background. In addition, custom elements often have no visible focus indicator at all. Ideal focus indicators are designed to provide good contrast with links and controls and their backgrounds. Focus indicators with good contrast make it much easier to track focus as a keyboard user navigates through the page."
    },
    {
        "ID": "412a",
        "Scenario": "Custom control is missing label/name",
        "CheckpointName": "4.1.2.a Name, Role, Value",
        "AssureCkpointID": "4.1.2.a Name, Role, Value",
        "AssureDetailsID": "NA",
        "IssueDescription": "Accessible name is not provided to the tab elements.",
        "Recommendation": "RULE:\\nThe name, role, value, states, and properties of user interface components MUST be programmatically determinable by assistive technologies.\\n\\nHOW TO FIX:\\nFix this issue using ONE of the following techniques:1.Use an aria-label attribute that conveys the the purpose or function of the control.\\n\\n<div class=\"confirm-modal\" role=\"dialog\" id=\"confirm-1\" aria-label=\"confirm identity\">\\n\\n2.Use an aria-labelledby attribute that references visible text on the screen that conveys the the purpose or function of the control.\\n\\n<div class=\"dqpl-radio\" role=\"radio\" aria-labelledby=\"yes\"></div>\\n<div class=\"dqpl-label\" id=\"yes\">Yes</div>\\n\\nREFERENCE:\\nDeque University: https://dequeuniversity.com/class/custom-widgets2/concepts/name\\nW3C ARIA Recommendation: https://www.w3.org/TR/wai-aria-1.1/#aria-label\\nW3C ARIA Recommendation: https://www.w3.org/TR/wai-aria-1.1/#aria-labelledby\\n\\nBACKGROUND:\\nEvery user interface control must have an accessible name that conveys the purpose or function of the control for screen reader and other assistive technology users."
    },
    {
        "ID": "412b",
        "Scenario": "Tab markup missing for the elements that appear as such.",
        "CheckpointName": "4.1.2.b custom controls",
        "AssureCkpointID": "4.1.2.b Custom Controls",
        "AssureDetailsID": "NA",
        "IssueDescription": "Element functions as tab but is not defined as such. Appropriate role is not provided.",
        "Recommendation": "RULE:\\nThe name, role, value, states, and properties of user interface components MUST be programmatically determinable by assistive technologies.\\n\\nHOW TO FIX:\\nFix this issue by using the following roles and attributes:\\n1.The element that serves as the container for the set of tabs has role tablist.\\n2.Each element that serves as a tab has role tab and is contained within the element with role tablist.\\n3.Each element that contains the content panel for a tab has role tabpanel.\\n4.Each element with role tab has the property aria-controls referring to its associated tabpanel element.\\n5.The active tab element has the state aria-selected set to true and all other tab elements have it set to false.\\n6.Each element with role tabpanel has the property aria-labelledby referring to its associated tab element.\\n7.If the tablist element is vertically oriented, it has the property aria-orientation set to vertical. The default value of aria-orientation for a tablist element is horizontal.\\n\\nNote: Keyboard accessibility MUST still be provided. As a best practice we strongly recommend following the ARIA Authoring Practices keyboard interaction pattern.\\nView the full ARIA design pattern including expected keyboard interaction and examples: https://www.w3.org/TR/wai-aria-practices-1.1/#tabpanel\\n\\nREFERENCE:\\nWAI-ARIA Authoring Practices: https://www.w3.org/TR/wai-aria-practices-1.1/#tabpanel\\nDeque University: https://dequeuniversity.com/class/custom-widgets2/examples/tab-panel\\n\\nBACKGROUND:\\nEvery user interface control must have a role along with any applicable states and properties so that screen reader users know how to interact with the control. Native HTML elements - such as <button>, <a>, <input>, <select> - already have a role, and their necessary states and properties - such as the checked/unchecked state of a checkbox - are automatically conveyed so nothing more needs to be done. If you create a custom version of a native HTML element or a custom control or widget that does not have a native HTML equivalent, you must add the relevant role(s) and any applicable states and properties using ARIA as well as expected keyboard interactions."
    }
]