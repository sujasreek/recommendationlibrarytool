﻿[
    {
        "ID": "143a",
        "Scenario": "Color contrast for regular text or link or image of text",
        "CheckpointName": "1.4.3.a Color Contrast (regular text)",
        "AssureCkpointID": "1.4.3.a Color Contrast (regular text)",
        "AssureDetailsID": "Text content lacks 4.5 to 1 contrast ratio",
        "IssueDescription": "Color contrast ratio on the tab elements is less than 4.5:1 with the background.",
        "Recommendation": "RULE:\\nSmall text (under 18 point regular font or 14 point bold font) MUST have a contrast ratio of at least 4.5 to 1 with the background.\\n\\nHOW TO FIX:\\nFix this issue by adjusting the text and/or background to increase the contrast to at least 4.5 to 1.\\n\\nREFERENCE:\\nDeque University: https://dequeuniversity.com/class/visual-design2/contrast/text-against-background\\n\\nBACKGROUND:\\nPeople who have low vision or are colorblind may have difficulty reading text if the contrast between the text its background is insufficient. When the contrast ratio between text and its background is adequate, people who have low vision or are colorblind are more likely to be able to read the text."
    },
    {
        "ID": "211a",
        "Scenario": "Keyboard operability missing for the elements",
        "CheckpointName": "2.1.1.a Keyboard Navigation",
        "AssureCkpointID": "2.1.1.a Keyboard Navigation",
        "AssureDetailsID": "Action is not accessible by keyboard alone",
        "IssueDescription": "Tabbed elements not keyboard operable.",
        "Recommendation": "RULE:\\nFunctionality MUST be available using the keyboard, unless the functionality cannot be accomplished in any known way using a keyboard.\\n\\nHOW TO FIX:\\nFix this issue by ensuring the component can be used by the keyboard.\\n\\nREFERENCE:\\nDeque University:https://dequeuniversity.com/class/input-methods2/keyboard-input/functionality\\n\\nBACKGROUND:\\nSome people cannot use a mouse due to vision or motor disabilities. Content that can be operated with a mouse must also be made operable with keyboard. When content is operable through a keyboard, it becomes operable by a variety assistive technologies such as speech input software, sip-and-puff software, on-screen keyboards, scanning software, and alternate keyboards."
    },
    {
        "ID": "212a",
        "Scenario": "Keyboard trap at an element",
        "CheckpointName": "2.1.2.a Keyboard Trap",
        "AssureCkpointID": "2.1.2.a Keyboard Trap",
        "AssureDetailsID": "Keyboard trap",
        "IssueDescription": "Keyboard trap exists within one tabbed element and its content.",
        "Recommendation": "RULE:\\nKeyboard focus MUST NOT be locked or trapped in a particular page element and the user MUST be able to navigate to and from all navigable page elements using only a keyboard.\\n\\nHOW TO FIX:\\nFix this issue by understanding where the keyboard traps is occurring and using a technique such as:\\n1.Make sure the way to navigate through a particular section of the page (usually the Tab key) can move past that section.\\n2.Create a keyboard stroke to exit the section (you must provide instructions for this to users).\\n\\nREFERENCE:\\nDeque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/keyboard-traps\\n\\nBACKGROUND:\\nSometimes custom widgets, plugins, embedded applications, and other content formats can create a keyboard \"trap\" where the keyboard focus gets stuck in one place or within a widget or application. Keyboard traps can make interacting with web content extremely difficult or impossible for keyboard users. Keyboard users must be able to move into, and away from, any user interface component, simply by using their keyboard. When keyboard traps are removed, people who rely on a keyboard will be able to access all of the otherwise-accessible content."
    },
    {
        "ID": "243a",
        "Scenario": "Incorrect focus order",
        "CheckpointName": "2.4.3.a Focus Order",
        "AssureCkpointID": "2.4.3.a Focus Order",
        "AssureDetailsID": "Tab order is not logical",
        "IssueDescription": "Tab order is not appropriate. Tabbing navigates from one element to another illogically.",
        "Recommendation": "RULE:\\nThe reading and navigation order MUST be logical and intuitive.\\n\\nHOW TO FIX:\\nFix this issue by ensuring the DOM order is logical so that the focus order will naturally follow. Do not use positive tabindex values to \"correct\" the tab order as it will not change the reading order for screen reader users and will almost always cause unintended, significant problems.\\n\\nREFERENCE:\\nDeque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/tab-order\\n\\nBACKGROUND:\\nWhen keyboard focusable components do not receive focus in a logical order, people with mobility impairments, reading disabilities, and low vision are all impacted. The keyboard focus order must be logical and consistent with the layout of the content. (Note: This does not mean that the focus order has to be identical to the visual order, as long as the user can still understand and operate the content.) A logical focus order makes interaction with content predictable for people who rely on a keyboard to interact with a web content."
    },
    {
        "ID": "247a",
        "Scenario": "Missing focus indicator",
        "CheckpointName": "2.4.7.a Focus Visible",
        "AssureCkpointID": "2.4.7.a Focus Visible",
        "AssureDetailsID": "Focus indicator is not clearly visible",
        "IssueDescription": "Visible focus indicator is not available on the elements while tabbing through them.",
        "Recommendation": "RULE:\\nAll focusable elements MUST have a visual focus indicator when in focus.\\nFocusable elements SHOULD have enhanced visual focus indicator styles.\\n\\nHOW TO FIX:\\nCommon keyboard focus indicator solutions include:\\n1.A 2px box around the focused element\\n2.A change in the background color of the focused element\\n3.The addition of an icon, such as an arrow, next to a menu item\\n4.The addition of a thick line under or next to the focused element\\n5.A change to the text of the component such as making it bold and/or underlined\\nA keyboard focus indicator can take different forms; it does not have to be a boring box. But the goal is to provide a difference in contrast between a component's default and focused states of at least 3 to 1.\\n\\nREFERENCE:\\nDeque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/visual-focus-indicator\\n\\nBACKGROUND:\\nWhen a visible keyboard focus indicator is not provided, sighted keyboard users will have no idea which link or control has focus making it extremely difficult, if not impossible, to interact with the content. Browsers provide default focus indicators for natively focusable elements, but these may be very difficult to see depending on the color of the control and the page background. In addition, custom elements often have no visible focus indicator at all. Ideal focus indicators are designed to provide good contrast with links and controls and their backgrounds. Focus indicators with good contrast make it much easier to track focus as a keyboard user navigates through the page."
    },
    {
        "ID": "412b",
        "Scenario": "Tab markup missing for the elements that appear as such.",
        "CheckpointName": "4.1.2.b Custom controls",
        "AssureCkpointID": "4.1.2.b Custom Controls",
        "AssureDetailsID": "NA",
        "IssueDescription": "Element functions as tab but is not defined as such.",
        "Recommendation": "RULE:\\nThe name, role, value, states, and properties of user interface components MUST be programmatically determinable by assistive technologies.\\n\\nHOW TO FIX:\\nFix this issue by using the following roles and attributes:\\n1.The element that serves as the container for the set of tabs has role tablist.\\n2.Each element that serves as a tab has role tab and is contained within the element with role tablist.\\n3.Each element that contains the content panel for a tab has role tabpanel.\\n4.Each element with role tab has the property aria-controls referring to its associated tabpanel element.\\n5.The active tab element has the state aria-selected set to true and all other tab elements have it set to false.\\n6.Each element with role tabpanel has the property aria-labelledby referring to its associated tab element.\\n7.If the tablist element is vertically oriented, it has the property aria-orientation set to vertical. The default value of aria-orientation for a tablist element is horizontal.\\n\\nNote: Keyboard accessibility MUST still be provided. As a best practice we strongly recommend following the ARIA Authoring Practices keyboard interaction pattern.\\nView the full ARIA design pattern including expected keyboard interaction and examples: https://www.w3.org/TR/wai-aria-practices-1.1/#tabpanel\\n\\nREFERENCE:\\nWAI-ARIA Authoring Practices: https://www.w3.org/TR/wai-aria-practices-1.1/#tabpanel\\nDeque University: https://dequeuniversity.com/class/custom-widgets2/examples/tab-panel\\n\\nBACKGROUND:\\nEvery user interface control must have a role along with any applicable states and properties so that screen reader users know how to interact with the control. Native HTML elements - such as <button>, <a>, <input>, <select> - already have a role, and their necessary states and properties - such as the checked/unchecked state of a checkbox - are automatically conveyed so nothing more needs to be done. If you create a custom version of a native HTML element or a custom control or widget that does not have a native HTML equivalent, you must add the relevant role(s) and any applicable states and properties using ARIA as well as expected keyboard interactions."
    }
]