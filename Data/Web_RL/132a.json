[
    {
        "ID": "132a",
        "AssureCkpointID":"1.3.2.a Reading Order",
        "SC": "1.3.2.a - Reading Order",
        "Details": "132a Reading Order"
    },
    {
        "IssueDescription": "Screen readers can read parent page content outside the modal using arrow keys",
        "AssureDescID": "NA",
        "RecShort": "Hide parent page content when modal is open",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Important note: If you do not place the modal dialog container as a direct child element of the <body> element in the DOM, handling the aria-hidden attribute becomes MUCH more difficult. You will be responsible for correctly adding the aria-hidden=\"true\" attribute to many more elements.\\n\\nFix this issue by adding the aria-hidden=\"true\" attribute to the modal dialog container’s sibling elements within the <body> element. (You do not need to add aria-hidden=\"true\" to <script> and <style> or the page overlay <div> elements as screen readers do not read content in those tags.) This will prevent screen reader users from browsing outside the modal dialog. When the modal dialog is dismissed it is extremely important that the aria-hidden=\"true\" attribute is removed to expose the page content to screen reader users again.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/within-pages/order",
        "Background": "Modal dialogs overlay page content and prevent users from interacting with the content behind the modal dialog until it is dismissed. If screen reader users can read content behind a modal dialog window, they may become disoriented or confused. Both keyboard focus and screen reader focus must be trapped within a modal dialog until it is dismissed. When both keyboard focus and browsing are trapped within a modal dialog, screen reader users are able to interact with it as intended."
    },
    {
        "IssueDescription": "New content is not in correct location",
        "AssureDescID": "New content is not in correct location",
        "RecShort": "Insert dynamic content in a logical location",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Fix this issue by ensuring that content that is added to the page is added, in the DOM, below the element or event that triggered the addition",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/within-pages/order",
        "Background": "People who are blind cannot see when content is dynamically inserted in a page. If content is inserted in a page above a screen reader user's point of reference, they may never be aware that is was added. Content that is added to a page must be added, in the DOM, below the element or event that triggered the addition. A screen reader user will then encounter the new information in the natural work flow or reading order of the content."
    },
    {
        "IssueDescription": "Disabled css reading order is not logical",
        "AssureDescID": "Disabled css reading order is not logical",
        "RecShort": "Ensure DOM order is logical",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Fix this issue by ensuring that the order of the content in the DOM is logical regardless of how it is visually arranged using CSS.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/within-pages/order",
        "Background": "Sometimes content must be read in a certain order to be understood. When screen reader users navigate page content, they hear the content in the order of the code in the DOM. If CSS is used to arrange content visually in a different order than it is presented in the DOM in such a way that meaning of content is changed, screen readers users may not understand the content correctly. The order of content in the DOM must be logical. The visual order of the content can differ from the source code order, as long as the reading order for screen readers is still logical and meaningful.\\n\\n"
    },
    {
        "IssueDescription": "Layout table reading order is not logical",
        "AssureDescID": "Layout table reading order is not logical",
        "RecShort": "Ensure DOM order is logical",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Fix this issue by ensuring that the order of the content in the DOM is logical regardless of how it is visually arranged in a layout table. This may be achieved by rethinking the way the layout table is structured, or (preferably) by using CSS to arrange content rather than a layout table.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/within-pages/order",
        "Background": "Sometimes content must be read in a certain order to be understood. When screen reader users navigate page content, they hear the content in the order of the code in the DOM. HTML tables are ordered in code with the left-most cell of the first row first, moving from cell-to-cell, left-to-right across the row, then the second row, left-to-right, and so on. Sometimes, when a layout table is used to arrange content, the way the content appears visually and the way it reads \"linearly\" by a screen reader differs in such a way that meaning of content is changed or is illogical. The order of content in the DOM must be logical and meaningful.\\n\\n"
    },
    {
        "IssueDescription": "Reading order is not logical/intuitive",
        "AssureDescID": "Reading order is not logical/intuitive",
        "RecShort": "Ensure DOM order is logical",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Fix this issue by ensuring that the order of the content in the DOM is logical.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/within-pages/order",
        "Background": "Sometimes content must be read in a certain order to be understood. When screen reader users navigate page content, they hear the content in the order of the code in the DOM. If the order of the code and the visual order of content differ in such a way that meaning of content is changed, screen readers users may not understand the content correctly. The order of content in the DOM must be logical. The visual order of the content can differ from the source code order, as long as the reading order for screen readers is still logical and meaningful.\\n\\n"
    },
    {
        "IssueDescription": "Hidden content is readable with a screen reader",
        "AssureDescID": "Hidden content is readable with a screen reader",
        "RecShort": "Remove elements from DOM or use CSS to hide them",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Fix this issue by removing the element from the DOM or hiding it from all users with CSS such as display:none.",
        "Reference": "Deque University: \\nhttps://dequeuniversity.com/class/semantic-structure2/within-pages/order",
        "Background": "When content that is meant to be hidden from all users - such as error messages or hidden submenus or dialogs or alerts - is still available to screen reader users, they may be mislead or confused about the meaning of the content. This may occur when techniques are used that hide content visibly but not from screen reader users.\\n"
    },
    {
        "IssueDescription": "Able to browse outside modal with screen reader",
        "AssureDescID": "Able to browse outside modal with screen reader",
        "RecShort": "Hide parent page content when modal is open",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Important note: If you do not place the modal dialog container as a direct child element of the <body> element in the DOM, handling the aria-hidden attribute becomes MUCH more difficult. You will be responsible for correctly adding the aria-hidden=\"true\" attribute to many more elements.\\n\\nFix this issue by adding the aria-hidden=\"true\" attribute to the modal dialog container’s sibling elements within the <body> element. (You do not need to add aria-hidden=\"true\" to <script> and <style> or the page overlay <div> elements as screen readers do not read content in those tags.) This will prevent screen reader users from browsing outside the modal dialog. When the modal dialog is dismissed it is extremely important that the aria-hidden=\"true\" attribute is removed to expose the page content to screen reader users again.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/within-pages/order",
        "Background": "Modal dialogs overlay page content and prevent users from interacting with the content behind the modal dialog until it is dismissed. If screen reader users can read content behind a modal dialog window, they may become disoriented or confused. Both keyboard focus and screen reader focus must be trapped within a modal dialog until it is dismissed. When both keyboard focus and browsing are trapped within a modal dialog, screen reader users are able to interact with it as intended.\\n\\n"
    }
]