[
    {
        "ID": "131b",
        "AssureCkpointID":"1.3.1.b Data Tables",
        "SC": "1.3.1.b - Data Table",
        "Details": "131b Data Table"
    },
    {
        "IssueDescription": "Content should be in a data table but is not",
        "AssureDescID": "Content should be in a data table but is not",
        "RecShort": "Use data table markup to present content",
        "Rule": "Tabular data SHOULD be represented in a <table>.",
        "HowToFix": "Fix this issue by doing ALL of the following:\\n1. Markup data that appears logically as a table using <table>, <tr>, and <td> elements.\\n2. Ensure that header cell and data cell relationships are correctly conveyed according to the complexity of the table.\\n3. If possible, add a <caption> element as the first child of the <table> element containing the name or title of the table.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels."
    },
    {
        "IssueDescription": "Data table has missing or incomplete header cell markup",
        "AssureDescID": "Data table has missing or incomplete header cell markup",
        "RecShort": "Use <th> markup on table headers",
        "Rule": "Table headers MUST be designated with <th>.",
        "HowToFix": "Fix this issue by doing ALL of the following:\\n1. Mark all column header and/or row header cells with a <th> element.\\n2. Ensure that header cell and data cell relationships are correctly conveyed according to the complexity of the table.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n"
    },
    {
        "IssueDescription": "Complex table headers-id association is incorrect",
        "AssureDescID": "Complex table headers-id association is incorrect",
        "RecShort": "Fix headers/id association",
        "Rule": "Header/data associations that cannot be designated with <th> and scope MUST be designated with headers plus id.",
        "HowToFix": "Fix this issue by ensuring that the association of data cells with their corresponding header cell(s) is accurate, following this pattern:\\n1. Mark every <th> element (header cell) with a unique id attribute value.\\n2. Mark every <td> element (data cell) with a headers attribute. The value of a data cell's headers attribute must be a space-separated list of all of its corresponding header cell id values.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/complex\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\nTables with a complex structure — in which data cells are merged or there are more than two levels of headers for any given dimension — require a different technique than simply marking the header cells with a scope of col, row, colgroup, or rowgroup. The headers / id technique explicitly associates each data cell with all of its corresponding header cells.\\n\\n"
    },
    {
        "IssueDescription": "Complex table is missing headers-id association",
        "AssureDescID": "Complex table is missing headers-id association",
        "RecShort": "Use headers/id method for complex table",
        "Rule": "Header/data associations that cannot be designated with <th> and scope MUST be designated with headers plus ids.",
        "HowToFix": "Fix this issue by doing ALL of the following:\\n1. Mark all header cells as <th> elements.\\n2. Mark every <th> element (header cell) with a unique id attribute value.\\n3. Mark every <td> element (data cell) with a headers attribute. The value of a data cell's headers attribute must be a space-separated list of all of its corresponding header cell id values.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/complex\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\nTables with a complex structure — in which data cells are merged or there are more than two levels of headers for any given dimension — require a different technique than simply marking the header cells with a scope of col, row, colgroup, or rowgroup. The headers / id technique explicitly associates each data cell with all of its corresponding header cells.\\n\\n"
    },
    {
        "IssueDescription": "First row of data table is really a caption",
        "AssureDescID": "First row of data table is really a caption",
        "RecShort": "Use <caption> element",
        "Rule": "The first row of a <table> MUST NOT be used to convey a table caption.",
        "HowToFix": "Fix this issue by doing ALL of the following:\\n1. Remove the spanned row from the table markup.\\n2. Use <caption> element before the table to display the caption.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/table-caption\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\nWhen a table merges all the cells in the first row to display what is essentially a caption screen readers cannot properly convey the table structure. \\n\\n"
    },
    {
        "IssueDescription": "Layout table uses data table structural markup",
        "AssureDescID": "Layout table uses data table structural markup",
        "RecShort": "Remove caption or summary",
        "Rule": "Layout tables MUST NOT contain data table markup.",
        "HowToFix": "Fix this issue by removing the <caption> element or summary attribute in tables that are used for layout purposes.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/layout-tables\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\n"
    },
    {
        "IssueDescription": "scope attribute is incorrect",
        "AssureDescID": "scope attribute is incorrect",
        "RecShort": "Use correct scope value",
        "Rule": "Table data cells MUST be associated with their corresponding header cells.\\nTable data group headers MUST be associated with their corresponding data cell groups.",
        "HowToFix": "Fix this issue by using the correct scope attribute value (row, col, rowgroup, or colgroup) on each <th> element.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/grouped\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\nThe scope attribute makes an explicit association between the table header cell and its corresponding data cells. If the incorrect scope value is used, for example it uses scope=\"row\" on column headers, then screen readers will read the table incorrectly.\\n\\n"
    },
    {
        "IssueDescription": "scope attribute is invalid",
        "AssureDescID": "scope attribute is invalid",
        "RecShort": "Use correct scope value",
        "Rule": "Only valid scope attribute values MUST be used.",
        "HowToFix": "Fix this issue by using the correct scope attribute value (row, col, rowgroup, or colgroup) on each <th> element as follows:\\n1. Apply the scope=\"col\" attribute to non-merged column headers.\\n2. Apply the scope=\"row\" attribute to non-merged row headers.\\n3. Apply the scope=\"colgroup\" attribute to all to merged column headers.\\n4. Apply the scope=\"rowgroup\" attribute to all merged column headers.\\n",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/grouped\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\n"
    },
    {
        "IssueDescription": "scope is used on <td>",
        "AssureDescID": "scope is used on <td>",
        "RecShort": "Change <td> with scope to <th>",
        "Rule": "The scope attribute MUST NOT be used on a <td> element.\\n",
        "HowToFix": "Fix this issue by changing each header cell that is marked as a <td> element with a scope attribute to a <th> element.\\n\\nNote: The scope attribute use on <td> elements is deprecated in HTML5. The scope attribute can be used only on <th> elements.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/grouped\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\n"
    },
    {
        "IssueDescription": "More than one table element used to create a single table",
        "AssureDescID": "More than one table element used to create a single table",
        "RecShort": "Code table as one single table",
        "Rule": "Data table headers and data associations MUST NOT be referenced across nested, merged, or separate tables. ",
        "HowToFix": "Fix this issue by recoding the table as one table marked with proper header cell and data cell relationships. Consider whether you can simplify the table structure.",
        "Reference": "Deque University: \\nhttps://dequeuniversity.com/class/semantic-structure2/tables/nested-split",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\nTables that are made of nested data tables or multiple data tables that appear as one table break the accessibility of the data presentation as a whole, making it impossible to associate the data and header cells appropriately. It is much better to create one or more simple tables than to create a confusing complex table that technically passes the accessibility guidelines."
    },
    {
        "IssueDescription": "Two data tables present when visually it is one data table",
        "AssureDescID": "NA",
        "RecShort": "Code table as one single table",
        "Rule": "Data table headers and data associations MUST NOT be referenced across nested, merged, or separate tables.",
        "HowToFix": "Fix this issue by recoding the table as one table marked with proper header cell and data cell relationships. Consider whether you can simplify the table structure.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/tables/nested-split\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/tables/",
        "Background": "People who are blind cannot see the organizational structure of a table with data arranged in rows and columns with corresponding header cells. In order for screen reader users to understand the logical relationships of data arranged in a table, tables need HTML markup that indicates header cells and data cells and defines their relationship. When tables are marked correctly, screen reader users are able to navigate data tables from cell to cell, in a multi-directional way (up, down, right, left), much like navigating a spreadsheet. As they move from cell to cell, screen readers will read the associated header labels.\\n\\nTables that are made of nested data tables or multiple data tables that appear as one table break the accessibility of the data presentation as a whole, making it impossible to associate the data and header cells appropriately. It is much better to create a simple table than to create a confusing complex table that technically passes the accessibility guidelines."
    }
]