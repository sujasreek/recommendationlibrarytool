﻿[
    {
        "ID": "243a",
        "AssureCkpointID":"2.4.3.a Focus Order",
        "SC": "2.4.3.a - Focus order",
        "Details": "243a Focus order"
    },
    {
        "IssueDescription": "Focus is not maintained in modal",
        "AssureDescID": "Focus is not maintained in modal",
        "RecShort": "Trap focus in the modal",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Fix this issue by using JavaScript to ensure the following keyboard interaction:\\n1.Tab key:\\na.Moves focus to the next focusable element inside the dialog.\\nb.If focus is on the last focusable element inside the dialog, moves focus to the first focusable element inside the dialog.\\n2.Shift + Tab keys:\\na.Moves focus to the previous focusable element inside the dialog.\\nb.If focus is on the first focusable element inside the dialog, moves focus to the last focusable element inside the dialog.\\n3.Escape key (best practice): Closes the dialog and returns focus to the triggering element or other logical element if the triggering element no longer exists.",
        "Reference": "Deque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/tab-order",
        "Background": "Modal dialogs overlay page content and prevent users from interacting with the content behind the modal dialog until it is dismissed. If sighted keyboard users or screen reader users can tab to content behind a modal dialog window, they may become disoriented or confused. Both keyboard focus and screen reader focus must be trapped within a modal dialog until it is dismissed. When both keyboard focus and browsing are trapped within a modal dialog, screen reader users are able to interact with it as intended."
    },
    {
        "IssueDescription": "Focus is not placed on opened modal",
        "AssureDescID": "Focus is not placed on opened modal",
        "RecShort": "Place the focus on to the newly opened modal",
        "Rule": "The focus MUST be purposely moved or set (via JavaScript) onto the appropriate element when the user's action requires a change of context or location for effective keyboard or touch interaction.",
        "HowToFix": "Fix this issue by moving focus to one of the following modal dialog elements (the content of the modal dialog will determine where focus should be placed):\\n1. Recommended method: The heading/title: If the modal dialog contains heading text, move focus directly to the heading text. This method can be used regardless of the type of content it contains as long as it has a visible heading/title. Be sure to add tabindex=\"-1\" to the heading/title container so JavaScript can put focus on it.\\n2. The first action button: If the modal dialog contains a brief amount of text (one short sentence) before the action buttons, you can move focus directly to the first active element. (If you do this, you will need to use the aria-describedby attribute appropriately on the role=\"dialog\" container to ensure the text content is read when the dialog is opened.)\\n3. The text content: If the modal dialog starts with text content (more than one short sentence) but does not have a visible heading/title, you can place focus on the text or first paragraph of text. Be sure to add tabindex=\"-1\" to the text container so JavaScript can put focus on it.\\n4. The first form field: If the modal dialog primarily exists to collect information from the user, you can move focus directly to the first form element. (If the modal dialog contains brief instructions before the form, you will need to use the aria-describedby attribute appropriately on the role=\"dialog\" container to ensure the instructions are read when the modal dialog is opened.)",
        "Reference": "Deque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/focus-management",
        "Background": "When a person who is blind opens a modal dialog and keyboard focus is not moved to it, it can be extremely difficult, if not impossible, to know that the modal dialog has appeared and to interact with it. Keyboard focus must be moved to an appropriate place in the modal dialog. This allows screen readers to announce the modal dialog and allow users to interact with the modal dialog with the keyboard."
    },
    {
        "IssueDescription": "Modal is closed, focus is not returned to trigger",
        "AssureDescID": "Modal is closed, focus is not returned to trigger",
        "RecShort": "Return focus to trigger or logical element",
        "Rule": "The focus MUST be purposely moved or set (via JavaScript) onto the appropriate element when the user's action requires a change of context or location for effective keyboard or touch interaction.\\nThe focus MUST NOT become lost or reset to the top of the page, except when loading or re-loading a page.",
        "HowToFix": "Fix this issue by placing focus back on the element that triggered the modal dialog or, if that element no longer exists, on another element that provides a logical workflow.",
        "Reference": "Deque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/focus-management",
        "Background": "When a modal dialog is closed, if focus is not explicitly placed back on an element, keyboard focus and screen reader focus will be lost - sometimes sending the focus back to the top of the DOM. Screen reader users may become disoriented, not hearing their new location and not knowing where their focus is. When a modal dialog is closed, focus must be placed back on the element that triggered the modal dialog or, if that element no longer exists, on another element that provides a logical workflow. This allows screen readers to announce the element that has received focus so screen reader users know where they are and allows keyboard users to interact logically with the content."
    },
    {
        "IssueDescription": "Hidden or empty element receives focus",
        "AssureDescID": "Hidden or empty element receives focus",
        "RecShort": "Hide item from keyboard or make visible",
        "Rule": "The reading and navigation order MUST be logical and intuitive.\\nAll focusable elements MUST have a visual focus indicator when in focus.",
        "HowToFix": "Fix this issue by using ONE of the following techniques:\\n1.If the element should not be visible or focusable, remove the element from the DOM or hide it from all users with CSS such as display:none.\\n2.If the element is meant to be visible but not interactive, make it visible and remove the focus state.\\n3.If the element is meant to be visible and interactive, then ensure that it is both focusable and visible to all users.",
        "Reference": "Deque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/tab-order\\nDeque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/visual-focus-indicator",
        "Background": "When an element that is not visible receives keyboard focus, sighted keyboard users and screen reader users may be left wondering if they are missing content or functionality. Every focusable element must have content and be visible. Then sighted keyboard users and screen reader users will know where their focus is and can be certain they are not missing content or functionality."
    },
    {
        "IssueDescription": "Use of positive tabindex value is not logical",
        "AssureDescID": "Use of positive tabindex value is not logical",
        "RecShort": "Remove positive tabindex, make DOM order logical",
        "Rule": "The reading and navigation order MUST be logical and intuitive.\\ntabindex of positive values SHOULD NOT be used.",
        "HowToFix": "Fix this issue by removing the positive tabindex value for the focusable elements and ordering the content appropriately in the DOM to align the focus order and the reading order.",
        "Reference": "Deque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/tab-order",
        "Background": "When keyboard focusable components do not receive focus in a logical order, people with mobility impairments, reading disabilities, and low vision are all impacted. The keyboard focus order must be logical and consistent with the layout of the content. (Note: This does not mean that the focus order has to be identical to the visual order, as long as the user can still understand and operate the content.) A logical focus order makes interaction with content predictable for people who rely on a keyboard to interact with web content."
    },
    
    {
        "IssueDescription": "Focus is lost or misplaced due to user interaction or content update",
        "AssureDescID": "Focus is lost or misplaced due to user interaction or content update",
        "RecShort": "Place the focus on to a logical element.",
        "Rule": "The focus MUST be purposely moved or set (via JavaScript) onto the appropriate element when the user's action requires a change of context or location for effective keyboard or touch interaction.",
        "HowToFix": "Fix this issue by explicitly placing focus on a logical element when content is removed, refreshed, or added, for example:\\n1. For content added to the screen in reaction to a user-fired event, focus should be shifted to the new content - such as in single page applications.\\n2. For content removed from the screen in reaction to a user-fired event, focus should be shifted to the next logical place in the interaction.",
        "Reference": "Deque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/focus-management\\nDeque University:\\nhttps://dequeuniversity.com/class/dynamic-updates2/notify-users/move-focus\\nDeque University: https://dequeuniversity.com/class/dynamic-updates2/ajax/single-page-applications",
        "Background": "One of the biggest challenges when creating rich web interfaces using JavaScript is the management of focus when new content or controls are added or removed from the page. The presentation or modification of content on the screen may require that the user interact with -or at the very least, take notice of - the new or changed content. It is important to have a clear indication of page content that has been updated. This allows the user to understand the change and also allows a keyboard user to interact with any new content. "
    },
    {
        "IssueDescription": "Programmatic focus does not move to intended target",
        "AssureDescID": "Programmatic focus does not move to intended target",
        "RecShort": "Move screen reader and visual focus to the intended element",
        "Rule": "The reading and navigation order MUST be logical and intuitive.",
        "HowToFix": "Fix the issue by moving both visual and screen reader focus to the intended element. For instance, when “return to top” link is activated, both visual and programmatic focus is moved to the top of the page.",
        "Reference": "Deque University: https://dequeuniversity.com/class/input-methods2/keyboard-input/tab-order",
        "Background": "When programmatic or keyboard focus does not follow visual focus - such as when an in-page link is coded to only shift the viewport - users of assistive technology do not get the same benefit as sighted mouse users. Programmatic and keyboard focus order must follow visual focus. A logical focus order makes interaction with content predictable for people who rely on a keyboard or screen reader to interact with a web content."
    }
]