﻿[
    {
        "ID": "131f",
        "AssureCkpointID":"1.3.1.f Lists",
        "SC": "1.3.1.f - List",
        "Details": "131f List"
    },
    
    {
        "IssueDescription": "Description list not marked up properly",
        "AssureDescID": "Description list not marked up properly",
        "RecShort": "Use <dl>, <dt>, and <dd> markup",
        "Rule": "Lists MUST be constructed using the appropriate semantic markup.",
        "HowToFix": "Fix this issue by using the definition list element (<dl>), to wrap one or more terms (<dt>) and associated description(s) (<dd>).\\n\\n<dl>\\n<dt>presentation</dt>\\n<dd>rendering of the content in a form to be perceived by users</dd>\\n<dt>relationships</dt>\\n<dd>meaningful associations between distinct pieces of content</dd>\\n</dl>",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/lists/semantic-markup\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/page-structure/content/#lists",
        "Background": "People who can see are able to look at a list and get a sense that it is a list, how large it is, and its structure - whether there are multiple lists, nested lists, etc. People who are blind do not have this ability if the list is not marked with semantic list markup. Lists must be marked semantically in a way that correctly identifies the list structure and type: unordered, ordered, or definition/description. When list markup is correctly applied, screen readers are able to notify users when they come to a list and tell them how many items are in a list.\\n\\n"
    },
    {
        "IssueDescription": "Visual list is not marked up as list",
        "AssureDescID": "Visual list is not marked up as list",
        "RecShort": "Use <ul>/<li> or <ol>/<li> markup",
        "Rule": "Lists MUST be constructed using the appropriate semantic markup.",
        "HowToFix": "Fix this issue by using ONE of the following techniques:\\n\\n1. Unordered list: Wrap a series of list items (<li>) inside an unordered list element (<ul>). Unordered lists should be used when a set of items can be placed in any order.\\n\\n<ul>\\n<li>Strawberries</li>\\n<li>Papaya</li>\\n<li>Mangos</li>\\n<li>Kiwis</li>\\n</ul>\\n\\n2. Ordered list: Wrap a series of list items (<li>) inside an ordered list element (<ol>). Ordered lists should be used when the list items need to be placed in a specific order.\\n\\n<h3>How to boil an egg</h3>\\n<ol>\\n<li>Place eggs in a large saucepan.</li>\\n<li>Cover them with cool water by 1 inch.</li>\\n<li>Cover pan with a lid and bring water to a rolling boil over high heat.</li>\\n<li>When the water has reached a boil, remove saucepan from the burner.</li>\\n<li>Let eggs sit in water for 12 minutes.</li>\\n</ol>",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/lists/semantic-markup\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/page-structure/content/#lists",
        "Background": "People who can see are able to look at a list and get a sense that it is a list, how large it is, and its structure - whether there are multiple lists, nested lists, etc. People who are blind do not have this ability if the list is not marked with semantic list markup. Lists must be marked semantically in a way that correctly identifies the list structure and type: unordered, ordered, or definition/description. When list markup is correctly applied, screen readers are able to notify users when they come to a list and tell them how many items are in a list.\\n\\n"
    },
    {
        "IssueDescription": "Nested list is not marked properly",
        "AssureDescID": "Nested list is not marked properly",
        "RecShort": "Nest list within listitem properly",
        "Rule": "Lists MUST be constructed using the appropriate semantic markup.",
        "HowToFix": "Fix this issue by nesting lists properly.\\n\\n<ul>\\n<li>Fruit\\n<ul>\\n<li>Strawberries</li>\\n<li>Mangos</li>\\n</ul>\\n</li>\\n<li>Vegetables\\n<ul>\\n<li>Tomatoes</li>\\n<li>Cucumbers</li>\\n</ul>\\n</li>\\n</ul>",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/lists/semantic-markup\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/page-structure/content/#lists",
        "Background": "People who can see are able to look at a list and get a sense that it is a list, how large it is, and its structure - whether there are multiple lists, nested lists, etc. People who are blind do not have this ability if the list is not marked with semantic list markup. Lists must be marked semantically in a way that correctly identifies the list structure and type: unordered, ordered, or definition/description. When list markup is correctly applied, screen readers are able to notify users when they come to a list and tell them how many items are in a list.\\n\\n"
    },
    {
        "IssueDescription": "List or list item is not marked up properly",
        "AssureDescID": "List or list item is not marked up properly",
        "RecShort": "Provide appropriate and correct list markup for the list item",
        "Rule": "Lists MUST be constructed using the appropriate semantic markup.",
        "HowToFix": "Fix this issue by correctly applying <ul>/<li> or <ol>/<li> markup.\\n1. Unordered list: Wrap a series of list items (<li>) inside an unordered list element (<ul>). Unordered lists should be used when a set of items can be placed in any order.\\n\\n<ul>\\n<li>Strawberries</li>\\n<li>Papaya</li>\\n<li>Mangos</li>\\n<li>Kiwis</li>\\n…\\n</ul>\\n\\n2. Ordered list: Wrap a series of list items (<li>) inside an ordered list element (<ol>). Ordered lists should be used when the list items need to be placed in a specific order.\\n\\n<h3>How to boil an egg</h3>\\n<ol>\\n<li>Place eggs in a large saucepan.</li>\\n<li>Cover them with cool water by 1 inch.</li>\\n<li>Cover pan with a lid and bring water to a rolling boil over high heat.</li>\\n<li>When the water has reached a boil, remove saucepan from the burner.</li>\\n<li>Let eggs sit in water for 12 minutes.</li>\\n</ol>",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/lists/semantic-markup\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/page-structure/content/#lists",
        "Background": "People who can see are able to look at a list and get a sense that it is a list, how large it is, and its structure - whether there are multiple lists, nested lists, etc. People who are blind do not have this ability if the list is not marked with semantic list markup. Lists must be marked semantically in a way that correctly identifies the list structure and type: unordered, ordered, or definition/description. When list markup is correctly applied, screen readers are able to notify users when they come to a list and tell them how many items are in a list.\\n\\n"
    },
    {
        "IssueDescription": "Content is not a list but is marked as such",
        "AssureDescID": "Content is not a list but is marked as such",
        "RecShort": "Remove list mark up",
        "Rule": "Semantic HTML elements MUST be used according to their semantic meaning, not to create a visual effect.",
        "HowToFix": "Fix this issue by removing the list markup.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/lists/semantic-markup\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/page-structure/content/#lists",
        "Background": "People who can see are able to look at a list and get a sense that it is a list, how large it is, and its structure - whether there are multiple lists, nested lists, etc. People who are blind do not have this ability if the list is not marked with semantic list markup. Lists must be marked semantically in a way that correctly identifies the list structure and type: unordered, ordered, or definition/description. When list markup is correctly applied, screen readers are able to notify users when they come to a list and tell them how many items are in a list."
    },
    {
        "IssueDescription": "List element has direct children that are not allowed inside <ul> or <ol> elements",
        "AssureDescID": "List element has direct children that are not allowed inside <ul> or <ol> elements",
        "RecShort": "<ul> or <ol> must only contain <li> , <script> or <template> elements as children",
        "Rule": "Lists MUST be constructed using the appropriate semantic markup",
        "HowToFix": "Fix this issue by ensuring that all ordered and unordered lists (defined by ul or ol elements) contain only <li>, <script> or <template> elements.",
        "Reference": "Deque University: \\nhttps://dequeuniversity.com/class/semantic-structure2/lists/semantic-markup",
        "Background": "People who can see are able to look at a list and get a sense that it is a list, how large it is, and its structure - whether there are multiple lists, nested lists, etc. People who are blind do not have this ability if the list is not marked with semantic list markup. The only elements that are valid as children of <ul> or <ol> elements are <li>, <script> or <template> elements. Including other elements may cause screen readers convey an incorrect number of list items."
    }
]