[
    {
        "ID": "131a",
        "AssureCkpointID":"1.3.1.a Semantics",
        "SC": "1.3.1.a - Semantics",
        "Details": "131a Semantics"
    },
    {
        "IssueDescription": "Form error and field relationship not identified",
        "AssureDescID": "Form error and field relationship not identified",
        "RecShort": "Specify the form field name in error message or associate the error message",
        "Rule": "An error message MUST identify (in text or programmatically) which field has the error.",
        "HowToFix": "Fix this issue by doing ONE or BOTH of the following:\\n1. Include the form element name in the error message.\\n\\n\"Error: Zip code must have 5 digits\"\\n\\n2. Programmatically associate the error message with the field using the aria-describedby attribute.\\n\\n<label for=\"zip\">Zip code</label>\\n<input type=\"text\" id=\"zip\" aria-describedby=\"zip-err\">\\n<div id=\"zip-err\">Error: Zip code must have 5 digits</div>",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/form-validation/error-identification\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual layout of a form to determine which form element an error message applies to. In order to be certain which form element an error message applies to, the error message must either contain the name of the form element with the error or be programmatically associated with the form element. When error messages and form elements are textually or programmatically associated, a screen reader user can be certain which form element an error message applies to.\\n\\n"
    },
    {
        "IssueDescription": "Quotation is missing semantics",
        "AssureDescID": "Quotation is missing semantics",
        "RecShort": "Use <blockquote> element",
        "Rule": "The <blockquote> element SHOULD be used to designate long (block level) quotations.\\nThe <blockquote> element SHOULD NOT be used for visual styling alone.",
        "HowToFix": "Fix this issue by wrapping the quotation in a <blockquote> element.\\n\\n<blockquote>\\n<p>Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition...</p>\\n</blockquote>",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/other-elements/blockquote-q",
        "Background": "People who are blind cannot use the visual formatting of a quotation to understand that an indented selection of text is a quotation. In order to semantically convey that a selection of text is a quotation, use the <blockquote> element. When quotations are semantically identified, a screen reader user will be able to understand that a selection of text is a quote."
    },
    {
        "IssueDescription": "aria-hidden=\"true\" is used incorrectly",
        "AssureDescID": "aria-hidden=\"true\" is used incorrectly",
        "RecShort": "Remove aria-hidden attribute",
        "Rule": "\\naria-hidden=\"true\" MUST NOT be used on content that screen readers need to access.",
        "HowToFix": "Fix this issue by removing the aria-hidden attribute.\\nNOTE: Using aria-hidden=\"false\" on content that is a decendent of an element that is hidden using aria-hidden=\"true\" will NOT expose that content to the accessibility API and it will not be accessible to screen readers or other assistive technologies.",
        "Reference": "ARIA Specification: https://www.w3.org/TR/wai-aria-1.1/#aria-hidden",
        "Background": "Using the aria-hidden=\"true\" attribute on an element removes the element and ALL of its child nodes from the accessibility API making it completely inaccessible to screen readers and other assistive technologies. Aria-hidden may be used with extreme caution to hide visibly rendered content from assistive technologies only if the act of hiding this content is intended to improve the experience for users of assistive technologies by removing redundant or extraneous content. If aria-hidden is used to hide visible content from screen readers, the identical or equivalent meaning and functionality must be exposed to assistive technologies.\\nNOTE: Using aria-hidden=\"false\" on content that is a decendent of an element that is hidden using aria-hidden=\"true\" will NOT expose that content to the accessibility API and it will not be accessible to screen readers or other assistive technologies."
    },
    {
        "IssueDescription": "Element inappropriately uses semantic markup",
        "AssureDescID": "Element inappropriately uses semantic markup",
        "RecShort": "Remove incorrect semantic markup",
        "Rule": "Semantic HTML elements MUST be used according to their semantic meaning, not to create a visual effect.",
        "HowToFix": "Fix this issue by removing incorrect semantic markup which is used for visual styling, and use CSS to create the visual effect desired such as using an <h2> element to make text bold or a <blockquote> element to indent text.\\n",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/intro",
        "Background": "People who are blind rely on their screen readers to correctly convey the purpose of a web element - such as a table, list, heading, blockquote, etc. When HTML elements and attributes are used incorrectly, for example to simply create a visual effect such as creating big bold text or indented text, a screen reader will incorrectly convey the purpose of the content. HTML elements must only be used according to their semantic meaning, not because of the way they appear visually. When HTML elements used correctly, screen reader users are able to correctly understand the purpose of a web element."
    },
    {
        "IssueDescription": "Group of navigation links missing semantics",
        "AssureDescID": "Group of navigation links missing semantics",
        "RecShort": "All techniques",
        "Rule": "Groups of navigation links MUST have semantics that convey their relationship.",
        "HowToFix": "Fix this issue by doing ONE OR MORE of the following:\\n1. Wrap the navigation area in the HTML <nav> element (preferred) or add role=\"navigation\" to the container wrapping the navigation area. If a page contains more than one navigation area, it is a best practice to include an aria-label to describe the type of navigation area - such as \"secondary\" or \"breadcrumb\" - for each section that is not the page's main navigation section.\\n2. Use list markup to provide semantic structure for the links.\\n3. Use heading markup to organize and describe the navigation area(s).\\n4. Use the ARIA menu design pattern.",
        "Reference": "Deque University: https://dequeuniversity.com/class/semantic-structure2/between-pages/navigation-lists\\nDeque University: https://dequeuniversity.com/class/semantic-structure2/lists/semantic-markup\\nDeque University: https://dequeuniversity.com/class/semantic-structure2/headings/real-headings\\nWAI-ARIA Authoring Practices: https://www.w3.org/TR/wai-aria-practices-1.1/#menu",
        "Background": "People who are blind cannot rely on the visual appearance and layout of a page to understand how links within a collection of links relate to each other or the page content. When navigation links appear in logical visual groups, their relationship must also be conveyed programmatically using semantic markup such as HTML list, heading, or <nav> elements, or ARIA landmark or menu markup. This will allow screen reader users to understand relationships that are apparent visually."
    },
    {
        "IssueDescription": "Role=\"presentation\" is used incorrectly",
        "AssureDescID": "Role=\"presentation\" is used incorrectly",
        "RecShort": "Remove role of presentation",
        "Rule": "role=\"presentation\" MUST NOT be used to override the semantics of an HTML element when the element requires the semantics for assistive technology.",
        "HowToFix": "Fix this issue by removing role=\"presentation\" from the element.",
        "Reference": "Deque University: https://dequeuniversity.com/class/custom-widgets2/concepts/role/presentation",
        "Background": "People who are blind rely on their screen readers to correctly convey the purpose of a web element - such as a table, list, heading, blockquote, etc. Marking an element with role=\"presentation\" essentially cancels the native HTML semantics / role of the element and turns it into the equivalent of a <span> or <div>, which are neutral, un-semantic elements that convey no role to a screen reader. Note: Adding role=\"presentation\" does NOT hide the element from anyone. Sighted users will still see it, and blind users will still hear the text, but their screen readers won't announce any kind of semantic role for the text. (An exception is adding role=\"presentation\" to an image will hide it from screen readers.)\\n\\nUse great caution when using role=\"presentation\" to hide an element's semantics. When HTML elements used correctly, screen reader users are able to correctly understand the purpose of a web element."
    }
]