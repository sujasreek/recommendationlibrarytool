[
    {
        "ID": "111c",
        "SC": "1.1.1.c - Complex Images",
        "AssureCkpointID":"1.1.1.c Alternative Text (Complex Images)",
        "Details": "111c Complex Images"
    },
    {
        "IssueDescription": "Detailed alternative description not adequate",
        "AssureDescID": "Detailed alternative description not adequate",
        "RecShort": "Provide meaningful long description",
        "Rule": "Complex images MUST be briefly described using alt text AND MUST have a more complete long description.",
        "HowToFix": "Fix this issue by ensuring that the long description for the image gives a full description of the content presented visually in the image.",
        "Reference": "Deque University: https://dequeuniversity.com/class/images2/alt-text/complex\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/images/",
        "Background": "People who are blind cannot see images on a page. In order to give people who cannot see an image access to the information conveyed by the image, it must have a text alternative. The text alternative must describe the information or function represented by the image. Screen readers can then use the alternative text to convey that information to the screen reader user.\\n\\nWhen a complex image - such as a chart or graph or infographic - cannot be described adequately in an alt attribute of approximately 250 characters or less, a longer description is required for screen reader users to understand all of the information presented visually. The alternative content can be presented within the page or on another, easily accessible, page."
    },
    {
        "IssueDescription": "Detailed alternative description is missing",
        "AssureDescID": "Detailed alternative description is missing",
        "RecShort": "All techniques",
        "Rule": "Complex images MUST be briefly described using alt text AND MUST have a more complete long description.",
        "HowToFix": "Fix this issue by using ONE of the following techniques:\\n\\n1. Provide the long description in the context of the HTML document itself.\\n2. Provide a button that expands a collapsed region that contains the long description.\\n3. Provide a button to open a dialog that contains the long description.\\n4. Provide a link to a long description on another page via a normal link text.",
        "Reference": "Deque University: https://dequeuniversity.com/class/images2/alt-text/complex\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/images/",
        "Background": "People who are blind cannot see images on a page. In order to give people who cannot see an image access to the information conveyed by the image, it must have a text alternative. The text alternative must describe the information or function represented by the image. Screen readers can then use the alternative text to convey that information to the screen reader user.\\n\\nWhen a complex image - such as a chart or graph or infographic - cannot be described adequately in an alt attribute of approximately 250 characters or less, a longer description is required for screen reader users to understand all of the information presented visually. The alternative content can be presented within the page or on another, easily accessible, page.\\n"
    },
    {
        "IssueDescription": "Short text alternative is missing",
        "AssureDescID": "Short text alternative is missing",
        "RecShort": "All techniques",
        "Rule": "Complex images MUST be briefly described using alt text AND MUST have a more complete long description.",
        "HowToFix": "Fix this issue using any of the following techniques:\\n\\n1. Use the alt attribute on the <img> element to provide a brief description of the image and a reference to the location of the long description.\\n\\n<img src=\"chart.jpg\" alt=\"Chart of sales results by quarter. Extended description is below the chart.\">\\n\\n2. Use the aria-label attribute on the image container to provide a brief description of the image and a reference to the location of the long description.\\n\\n<div role=\"img\" aria-label=\"Map of Italy. Extended description is below.\">\\n<img src=\"map-portion-1.jpg\" alt=\"\">\\n<img src=\"map-portion-2.jpg\" alt=\"\">\\n<img src=\"map-portion-3.jpg\" alt=\"\">\\n</div>",
        "Reference": "Deque University: https://dequeuniversity.com/class/images2/alt-text/complex\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/images/",
        "Background": "People who are blind cannot see images on a page. In order to give people who cannot see an image access to the information conveyed by the image, it must have a text alternative. The text alternative must describe the information or function represented by the image. Screen readers can then use the alternative text to convey that information to the screen reader user.\\n\\nEven when a long description is provided for a complex image like a chart or graph or infographic, the image's brief alternative text should describe the main purpose or content of the image and reference the location of the long description.\\n\\n"
    },
    {
        "IssueDescription": "Short text alternative is not appropriate",
        "AssureDescID": "Short text alternative is not appropriate",
        "RecShort": "Provide meaningful alternative text",
        "Rule": "Complex images MUST be briefly described using alt text AND MUST have a more complete long description.\\n\\nThe alternative text for informative images MUST be meaningful (accurately conveying the purpose of the image, and the author's intent).",
        "HowToFix": "Fix this issue by ensuring that the short alternative text for the image gives a meaningful description of the main purpose or content of the image and refers to the location of the long description.\\n\\n<img src=\"chart.jpg\" alt=\"Sales results by quarter. Extended description is below the chart.\">",
        "Reference": "Deque University: https://dequeuniversity.com/class/images2/alt-text/complex\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/images/",
        "Background": "People who are blind cannot see images on a page. In order to give people who cannot see an image access to the information conveyed by the image, it must have a text alternative. The text alternative must describe the information or function represented by the image. Screen readers can then use the alternative text to convey that information to the screen reader user.\\n\\nEven when a long description is provided for a complex image like a chart or graph or infographic, the image's brief alternative text should describe the main purpose or content of the image and reference the location of the long description.\\n\\n"
    }
]