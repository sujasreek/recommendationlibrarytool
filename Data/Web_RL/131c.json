[
    {
        "ID": "131c",
        "AssureCkpointID":"1.3.1.c Programmatic Labels",
        "SC": "1.3.1.c - Programmatic labels",
        "Details": "131c Programmatic labels"
    },
    {
        "IssueDescription": "Label in <th> not associated with form control",
        "AssureDescID": "Label in <th> not associated with form control",
        "RecShort": "Use aria-labelledby",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using an aria-labelledby attribute on the <input> to reference the label in another cell. The value of the aria-labelledby attribute is the id attribute value of the visible text label.\\n\\nIMPORTANT: Note that the table headers/labels must be in <span> elements and that it is the <span> element and NOT the <th> element that has the id. If you place the id on the <th> element, some screen readers will not read the labels correctly when you tab through the form elements. ",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nDeque University: https://dequeuniversity.com/class/forms2/labels/multiple-labels-for-one\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual layout of a form to determine which labels go with which form elements. In order to be certain which label goes with which form element, the label and form element must be programmatically associated. When labels and form elements are programmatically associated, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\nWhen a form is laid out using table markup, visually sighted users identify the purpose of the form field through label placement in the table's header cells. The same information is not being communicated to screen reader users as the label in the header cells association is not programmatically discernable. This causes the form field's label association to not be announced to screen reader users.\\n\\n"
    },
    {
        "IssueDescription": "Hidden form control has a label",
        "AssureDescID": "Hidden form control has a label",
        "RecShort": "Hide or remove label",
        "Rule": "Hidden form controls MUST NOT have labels.",
        "HowToFix": "Fix the issue by removing the form control label.",
        "Reference": "NA",
        "Background": "NA"
    },
    {
        "IssueDescription": "Form field is not labeled",
        "AssureDescID": "Form field is not labeled",
        "RecShort": "All Techniques",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using ONE of the following techniques:\\n1. Explicit label: Under most circumstances, the best technique is to use the <label> element with the for attribute. The value of the for attribute is the id attribute value of the <input> element.\\n<label for=\"fname\">First Name:</label>\\n<input type=\"text\" name=\"fn\" id=\"fname\">\\n2. Use an aria-label attribute or title attribute on the <input> to provide a label when there is no visible label.\\n<input type=\"text\" aria-label=\"search\">\\n<input type=\"submit\" value=\"Search\">\\n3. Use an aria-labelledby attribute on the <input> to reference a visible label. The value of the aria-labelledby attribute is the id attribute value of the visible text label.\\n<span id=\"nickname\">Nickname:</span>\\n<input type=\"text\" aria-labelledby=\"nickname\">\\n4. Implicit label (explicit label method is strongly preferred): Wrap the form element within the <label> element.\\n<label>First Name: <input type=\"text\" name=\"fn\"></label>",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual presentation of a form to determine the label for a form element. In order for screen reader users to be certain of a form field's label, every form input and control needs a label, also known as an \"accessible name.\" When form elements have a programmatically determinable accessible name, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Form field is not labeled",
        "AssureDescID": "Form field is not labeled",
        "RecShort": "use label/for",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using the <label> element with the for attribute. The value of the for attribute is the id attribute value of the <input> element.\\n<label for=\"fname\">First Name:</label>\\n<input type=\"text\" name=\"fn\" id=\"fname\">",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual presentation of a form to determine the label for a form element. In order for screen reader users to be certain of a form field's label, every form input and control needs a label, also known as an \"accessible name.\" When form elements have a programmatically determinable accessible name, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Form field is not labeled",
        "AssureDescID": "Form field is not labeled",
        "RecShort": "Use aria-label or title",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using an aria-label attribute (preferred) or title attribute on the <input> to provide a label when there is no visible label.\\n<input type=\"text\" aria-label=\"search\">\\n<input type=\"submit\" value=\"Search\">",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual presentation of a form to determine the label for a form element. In order for screen reader users to be certain of a form field's label, every form input and control needs a label, also known as an \"accessible name.\" When form elements have a programmatically determinable accessible name, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Form field is not labeled",
        "AssureDescID": "Form field is not labeled",
        "RecShort": "use aria-labelledby",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using an aria-labelledby attribute on the <input> to reference a visible label. The value of the aria-labelledby attribute is the id attribute value of the visible text label.\\n<span id=\"Nname\">Nickname:</span>\\n<input type=\"text\" aria-labelledby=\"Nname\">",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual presentation of a form to determine the label for a form element. In order for screen reader users to be certain of a form field's label, every form input and control needs a label, also known as an \"accessible name.\" When form elements have a programmatically determinable accessible name, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Visible label/form field are not associated",
        "AssureDescID": "Visible label/form field are not associated",
        "RecShort": "All Techniques",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using ONE of the following techniques:\\n1. Explicit label: Under most circumstances, the best technique is to use the <label> element with the for attribute. The value of the for attribute is the id attribute value of the <input> element.\\n\\n<label for=\"fname\">First Name:</label>\\n<input type=\"text\" name=\"fn\" id=\"fname\">\\n\\n2. Use an aria-label attribute or title attribute on the <input> to provide a label when there is no visible label.\\n\\n<input type=\"text\" aria-label=\"search\"> <input type=\"submit\" value=\"Search\">\\n\\n3. Use an aria-labelledby attribute on the <input> to reference a visible label. The value of the aria-labelledby attribute is the id attribute value of the visible text label.\\n\\n<span id=\"nickname\">Nickname:</span> <input type=\"text\" aria-labelledby=\"nickname\">\\n\\n4. Implicit label (explicit label method is strongly preferred): Wrap the form element within the <label> element.\\n<label>First Name: <input type=\"text\" name=\"fn\"></label>",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual layout of a form to determine which labels go with which form elements. In order to be certain which label goes with which form element, the label and form element must be programmatically associated. When labels and form elements are programmatically associated, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Visible label/form field are not associated",
        "AssureDescID": "Visible label/form field are not associated",
        "RecShort": "use label/for",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using the <label> element with the for attribute. The value of the for attribute is the id attribute value of the <input> element.\\n\\n<label for=\"fname\">First Name:</label>\\n<input type=\"text\" name=\"fn\" id=\"fname\">",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual layout of a form to determine which labels go with which form elements. In order to be certain which label goes with which form element, the label and form element must be programmatically associated. When labels and form elements are programmatically associated, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Visible label/form field are not associated",
        "AssureDescID": "Visible label/form field are not associated",
        "RecShort": "Use aria-label or title",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using an aria-label attribute (preferred) or title attribute on the <input> to provide a label when there is no visible label.\\n\\n<input type=\"text\" aria-label=\"search\">\\n<input type=\"submit\" value=\"Search\">",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual layout of a form to determine which labels go with which form elements. In order to be certain which label goes with which form element, the label and form element must be programmatically associated. When labels and form elements are programmatically associated, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Visible label/form field are not associated",
        "AssureDescID": "Visible label/form field are not associated",
        "RecShort": "Use aria-labelledby",
        "Rule": "Labels MUST be programmatically associated with their corresponding elements.",
        "HowToFix": "Fix this issue by using an aria-labelledby attribute on the <input> to reference a visible label. The value of the aria-labelledby attribute is the id attribute value of the visible text label.\\n\\n<span id=\"Nname\">Nickname:</span>\\n<input type=\"text\" aria-labelledby=\"Nname\">",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/semantic-labels\\nW3C-WAI tutorial: https://www.w3.org/WAI/tutorials/forms/",
        "Background": "People who are blind cannot use the visual layout of a form to determine which labels go with which form elements. In order to be certain which label goes with which form element, the label and form element must be programmatically associated. When labels and form elements are programmatically associated, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together. In addition, some coding methods will create a larger clickable area for the form element which benefits people with motor disabilities.\\n\\n"
    },
    {
        "IssueDescription": "Form field has multiple <label> elements associated",
        "AssureDescID": "Form field has multiple <label> elements associated",
        "RecShort": "Use aria-labelledby",
        "Rule": "When multiple labels are used for one element, each label MUST be programmatically associated with the corresponding element.\\nA <label> and a form element must have a one-to-one relationship.",
        "HowToFix": "While HTML5 allows a single form element to be referenced by more than one <label> element via the for attribute, this technique is not well supported by screen readers. As a result, most screen reader users will only hear one label.\\n\\n\\nFix this issue by using an aria-labelledby attribute on the input to reference multiple visible labels. The value of the aria-labelledby attribute is a space-separated list of the id attribute values of each visible text label.\\n\\n<label id=\"label1\">Cat</label><input type=\"text\" aria-labelledby=\"label1 label2\" name=\"cat\"><br>\\n<label id=\"label2\">Enter your cat's name followed by your last name</label>",
        "Reference": "Deque University: https://dequeuniversity.com/class/forms2/labels/multiple-labels-for-one",
        "Background": "People who are blind cannot use the visual layout of a form to determine which labels go with which form elements. In order to be certain which label goes with which form element, the label and form element must be programmatically associated. When labels and form elements are programmatically associated, a screen reader user can put focus on a form element and the screen reader will automatically read the label and element type together."
    }
]