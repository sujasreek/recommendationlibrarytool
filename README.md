## How to access the JSON data?

- Login to `vpn.dequecloud.com` through watchguard and conntect to `a11ytest.dequelabs.com` server
- On successful login, go to `c:\inetpub\ATFS\DQLibraries\Production\` to view respective folders

## How to add a new checkpoint?

**Adding Checkpoint**

Open `RecIndex.JSON` files and add the checkpoint in the index file

**Example:**

```json
{
    "ID": "111a",
    "SC": "1.1.1.a - Active Images",
    "Devices":"desktop-web,native-ios,native-android"
}
```

Once the checkpoint is added, we can see the newly added checkpoint under checkpoint combo box in RRL

## How to descriptions for the checkpoints?

To add checkpoint descriptions, create a new file with `ID` which is provided in the `RecIndex.json` file. In the checkpoint provide the basic checkpoint details and checkpoint descriptions.

**Basic Details**

```json
{
        "ID": "111a",
        "SC": "1.1.1.a - Active Images",
        "AssureCkpointID":"1.1.1.a Alternative Text (Active Images)",
        "Details": ""
    }
```

**Checkpoint Descriptions**

```json
{
        "IssueDescription": "Active image missing alternative text",
        "AssureDescID": "Alt text is missing",
        "RecShort": "All techniques",
        "Rule": "....",
        "HowToFix": ".....",
        "Reference": ".....",
        "Background": "....."
    }
```

**Note:** Make sure that `AssureDesID` is same as description provided in the Assure description combo box.

## How to deploy the application?

To deploy the application login to google developer console (You can use your google account to deploy the developer console)

Current RRL dev console URL: https://chrome.google.com/webstore/devconsole/g07413333979813933329/bjaapmgebcchgfahiieackkmaoemdjen/edit?hl=en_US

On successful login, select the extension from the list of extensions.

On extensions details screen, select the package from the left menu and upload the latest version of the extension.

**Note:** Make sure that the version number in the new upload code (you can find the version number in manifest file) is grater than existing extension version which is deployed.

Once the package is uploaded successfully, click on publish button.



