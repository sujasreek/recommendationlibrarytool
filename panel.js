// Global Variables 
var _gRL,IssueDescriptions,recfor=0,_gIssueDescriptions,_WL,Widgets,WidgetIssueLength= [],recforName="desktop-web";

window.onload = populateSelect();
// Fetch Recommendation checkpoints index JSON file from URL
function populateSelect() { 
    var xhr = new XMLHttpRequest();
    //xhr.open("GET", "http://a11ytest.dequelabs.com/DQLibraries/RecIndex.json", false);
    xhr.open("GET", "http://a11ytest.dequelabs.com/DQLibraries/Production/RecIndex.json", false);
    xhr.send();
    _gRL = JSON.parse(xhr.responseText); 
    bindSCs(recforName);
    bindAllWidgets();
}
$("#recfor").on('change',function(e){
    recfor=$(this).find("option:selected").val();
    $('#rectext,#selIssueDes,#upsummaryctrl,#reclist').empty();
    $('#SC').val("0");          
    $('#rectext,#selIssueDes').append('<option value="0">--SELECT--</option>');
    recforName=$(this).find("option:selected").attr('name');
    bindSCs(recforName);
});
// Load Success Criterials in Recommendation tab
function bindSCs(ddlname){
    $('#SC').empty().append('<option value="0">--SELECT--</option>');
    var _scids = [],SCs = [];
    $.each(_gRL, function(index, value) {
        if($.inArray(value.SC, _scids) == -1)
        {
            _scids.push(value.SC);
            SCs.push(value);
        }
    });
    var ele = document.getElementById('SC');
    for (var i = 0; i < SCs.length; i++) {
        // POPULATE SELECT ELEMENT WITH JSON.
        if(ele.innerHTML!=null)
        if(~SCs[i].Devices.indexOf(ddlname)){
        ele.innerHTML = ele.innerHTML +'<option value='+SCs[i].ID+'>' + SCs[i].SC + '</option>';
        }
    }
}
// Bind Issue Descriptions based on success criteria change in recommendation tab
function bindIssueDescription(dirName,checkpointID){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://a11ytest.dequelabs.com/DQLibraries/Production/"+dirName+"/"+checkpointID+".json", false);
    xhr.send();
    IssueDescriptions = JSON.parse(xhr.responseText); 
    $("#scenario").empty();
    var _scids = [],IssueDescription = [],hasRec=false;
    $.each(IssueDescriptions, function(index, value) {
        if(value.HowToFix!="NA"){
        if($.inArray(value.IssueDescription, _scids) == -1)
        {
            _scids.push(value.IssueDescription);
            IssueDescription.push(value);
        }
    }
    });
    var selIssueDes = document.getElementById('selIssueDes'),
    rectext = document.getElementById('rectext');
    selIssueDes.innerHTML = selIssueDes.innerHTML +'<option value="0">--SELECT--</option>';
    rectext.innerHTML = rectext.innerHTML +'<option value="0">--SELECT--</option>';
    for (var i = 0; i < IssueDescription.length; i++) {
        // POPULATE SELECT ELEMENT WITH JSON.
        if(selIssueDes.innerHTML!=null && IssueDescription[i].IssueDescription!=null)
        selIssueDes.innerHTML = selIssueDes.innerHTML +'<option value='+IssueDescription[i].ID+'>' + IssueDescription[i].IssueDescription.replace(/\</g, '&lt;').replace(/\>/g, '&gt;') + '</option>';
    }
}
// Bind Techniques based on IssueDescription change in recommendation tab
function bindrecommendationbytype(selectedIssueDescription){
    var _ids = [],RecType = [];
    $.each(IssueDescriptions, function(index, value) {
        if($.inArray(value.RecShort, _ids) == -1)
        {
            _ids.push(value.IssueDescription);
            RecType.push(value);
        }
    });
    var rectext = document.getElementById('rectext');
    rectext.innerHTML = rectext.innerHTML +'<option value="0">--SELECT--</option>';
    for (var i = 0; i < RecType.length; i++) {
        // POPULATE SELECT ELEMENT WITH JSON.
        if(rectext.innerHTML!=null && RecType[i].IssueDescription==selectedIssueDescription)
        if(RecType[i].HowToFix !="NA")
        rectext.innerHTML = rectext.innerHTML +'<option>' + RecType[i].RecShort.replace(/\</g, '&lt;').replace(/\>/g, '&gt;') + '</option>';
    }
}

// Get Recommnedation data based on technique change in recommendation tab
function getRecommendations(_IssueDescription,technique,recfor){    
    for (var i=0;i<IssueDescriptions.length;i++){
    if(IssueDescriptions[i].RecShort==technique && IssueDescriptions[i].IssueDescription==_IssueDescription){
        if(recfor==0){
            var _RecShort=IssueDescriptions[i].RecShort.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\''),    
                _Rule=IssueDescriptions[i].Rule.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\''),
                _HowToFix=IssueDescriptions[i].HowToFix.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\''),
                _Reference=IssueDescriptions[i].Reference.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\''),
                _Background=IssueDescriptions[i].Background.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\'');
                //$('#upsummaryctrl').html("<button id='sumbtn' class='btn btn-primary' onclick='updateSummary()' style='margin-top:-3px'>Update Summary</button>");
                var _IssueDeslist;
                _IssueDeslist="<div class='panel'><div class='panel-heading panel-blue'>"+
                "Recommendation <div id='rec_issueheading_"+i+"' role='heading' aria-level='1' style='font-size:16px;margin:0px !important'>"+ _RecShort+" </div><span class='pull-right'><button class='recommendation btn btn-default btn-sm' id='"+i+"' aria-describedby='rec_issueheading_"+i+"'>Copy to Assure</button><button id='copyrec_"+i+"' aria-describedby='rec_issueheading_"+i+"' style='margin-left:2px' data-clipboard-action='copy' data-clipboard-target='#contenttocopy_"+i+"' class='copytoclipboard btn btn-default btn-sm'>copy to clipboard</button></span></div>"+
                "<div class='panel-body'><br/>"+
                "<div id='contenttocopy_"+i+"'>"+
                "<strong role='heading' aria-level='2'>RULE :</strong><p id='rulecontent'>"+_Rule.replace(/\\n/g, '<br/>')+"</p><br/>"+ 
                "<strong role='heading' aria-level='2'>HOW TO FIX :</strong><p id='howtofixcontent'>"+_HowToFix.replace(/\\n/g, '<br/>')+"</p><br/>"+
                "<strong role='heading' aria-level='2'>REFERENCE :</strong><p id='referencecontent'>"+_Reference.replace(/\\n/g, '<br/>')+"</p><br/>"+
                "<strong role='heading' aria-level='2'>BACKGROUND :</strong><p id='backgroundcontent'>"+_Background.replace(/\\n/g, '<br/>')+"</p><br/>"+
                "</div><span class='hidden checkpointid'>"+IssueDescriptions[0].AssureCkpointID+"</span><span id='recdescriptionID_"+i+"' class='hidden'>"+IssueDescriptions[i].AssureDescID.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\'').replace(/\&/g, '&amp;')+"</span></div></div>"
                $("#reclist").append(_IssueDeslist);
            }
            else{
                var headings = ['RULE:','HOW TO FIX:','REFERENCE:','BACKGROUND:'];
                var reg = RegExp(headings.join('|'), 'gi');
                var _RecShort=IssueDescriptions[i].RecShort.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\''),    
                _HowToFix=IssueDescriptions[i].HowToFix.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\'');
                var _IssueDeslist;
                _IssueDeslist="<div class='panel'><div class='panel-heading panel-blue'>"+
                "Recommendation <div id='rec_issueheading_"+i+"' role='heading' aria-level='1' style='font-size:16px;margin:0px !important'>"+ _RecShort+" </div><span class='pull-right'><button class='recommendation btn btn-default btn-sm' id='"+i+"' aria-describedby='rec_issueheading_"+i+"'>Copy to Assure</button><button id='copyrec_"+i+"' aria-describedby='rec_issueheading_"+i+"' style='margin-left:2px' class='copytoclipboard btn btn-default btn-sm'>copy to clipboard</button></span></div>"+
                "<div class='panel-body'><br/>"+
                "<div id='contenttocopy_"+i+"'>"+
                "<p id='howtofixcontent'>"+(_HowToFix.replace(/\\n/g, '<br/>')).replace(reg, '<strong>$&</strong>')+"</p><br/>"+
                "</div><span class='hidden checkpointid'>"+IssueDescriptions[0].AssureCkpointID+"</span><span id='recdescriptionID_"+i+"' class='hidden'>"+IssueDescriptions[i].AssureDescID.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\'>'/g, '\\\'').replace(/\&/g, '&amp;')+"</span></div></div>"
                $("#reclist").append(_IssueDeslist);
                }
            }
        }
    }

// Trigger success criteria change in recommendation tab
$("#SC").on('change',function(e){
    $("#selIssueDes,#reclist,#scenarioDetails,#rectext").empty();
    $("#message").removeAttr().removeClass().empty();
    $('#searchsec').addClass("hidden");
    var sc = document.getElementById('SC'),
        technique = document.getElementById('rectext'),dirName="NA";
    if($('#recfor').val()=="0"){
    dirName="Web_RL"; // Provide Web Directory Name
    bindIssueDescription(dirName,sc.options[sc.selectedIndex].value);}
    else if($('#recfor').val()=="1"){
    dirName="NIOS_RL"; // Provide Native IOS Directory Name
    bindIssueDescription(dirName,sc.options[sc.selectedIndex].value);}
    else if($('#recfor').val()=="2"){
    dirName="NAndroid_RL"; // Provide Native IOS Directory Name
    bindIssueDescription(dirName,sc.options[sc.selectedIndex].value);}

});

// Trigger Scenario change in recommendation tab

$("#selIssueDes").on('change',function(e){
    $("#message").removeAttr().removeClass().empty();
    $("#reclist,#rectext").empty(); 
    var _IssueDescription = document.getElementById('selIssueDes'),
        technique = document.getElementById('rectext');
        _gIssueDescriptions=_IssueDescription.options[_IssueDescription.selectedIndex].text;
    bindrecommendationbytype(_IssueDescription.options[_IssueDescription.selectedIndex].text);
});

// Trigger Technique change in recommendation tab

$("#rectext").on('change',function(e){
    $("#reclist").empty(); 
    $("#message").removeAttr().removeClass().empty();
    var technique = document.getElementById('rectext'),
    _IssueDescription = document.getElementById('selIssueDes');
    getRecommendations(_IssueDescription.options[_IssueDescription.selectedIndex].text,technique.options[technique.selectedIndex].text,recfor);
});

// Attach click event to copy to assure button in recommendation tab

$(document).on('DOMNodeInserted','.panel', function() { 
    var bns = document.getElementsByClassName("recommendation"),
        clipboardbtn= document.getElementsByClassName("copytoclipboard");
    for (i = 0; i < bns.length; i++) {
      bns[i].addEventListener('click',reply_click, false);
      clipboardbtn[i].addEventListener('click',copytoClipboard,false);
}
});

// Attach click event to update summary button in recommendation tab

$(document).on('DOMNodeInserted','#sumbtn', function() { 
    var sumbtn=document.getElementById("sumbtn");
    sumbtn.addEventListener('click',updateSummary, false);
});

// sending the code to the page to update summary from selected description

function updateSummary(){
    sendObjectToInspectedPage({action: "script", content: "messageback-script.js"});
}

//copy recommendation to clipboard
function copytoClipboard(event){
    $("#message").empty().removeClass('alert alert-success');
    var btnID=event.target.id;
    btnID.split('_')[0]=='copyrec' ? copywithformat('contenttocopy_'+btnID.split('_')[1]) : copywithformat('contenttocopywdgt_'+btnID.split('_')[1]);
    $("#message").attr('role','alert').addClass('alert alert-success').html("Content is copied to clipboard");
    setTimeout(function(){$("#message").empty().removeClass('alert alert-success');},5000);
}

function copywithformat(cointainerID){
    if (window.getSelection) {
        if (window.getSelection().empty) { // Chrome
            window.getSelection().empty();
        }

        if (document.selection) { 
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(cointainerID));
            range.select().createTextRange();
            document.execCommand("copy"); 
        
        } else if (window.getSelection) {
            var range = document.createRange();
             range.selectNode(document.getElementById(cointainerID));
             window.getSelection().addRange(range);
             document.execCommand("copy");
            }
        }
}

// sending the recommendation, checkpoint ID, description ID to web page
function reply_click(event)
{
    $("#message").removeAttr().removeClass().empty();
    var btnID=event.target.id, 
        _rule=(recfor==0?$('#'+btnID).parents('.panel').find('.panel-body #rulecontent').html().replace(/\<br>/g, '\\n').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/'/g, "\\'"):""),
        _howtofix=$('#'+btnID).parents('.panel').find('.panel-body #howtofixcontent').html().replace(/\<br>/g, '\\n').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/'/g, "\\'"),
        _reference=(recfor==0?"\\nREFERENCE:\\n"+$('#'+btnID).parents('.panel').find('.panel-body #referencecontent').html().replace(/\<br>/g, '\\n').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/'/g, "\\'"):""),
        _background=(recfor==0?"\\nBACKGROUND:\\n"+$('#'+btnID).parents('.panel').find('.panel-body #backgroundcontent').html().replace(/\<br>/g, '\\n').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/'/g, "\\'"):""),
        complete_recommendation="";
        if(recfor==0)
        complete_recommendation="RULE:\\n"+_rule+"\\n\\nHOW TO FIX:\\n"+_howtofix+"\\n"+_reference+"\\n"+_background;
        else
        complete_recommendation=_howtofix.replace(/<\/?strong>/g, "");
        assurecheckpointID=$('#'+btnID).parents('.panel').find('.panel-body .checkpointid').html(),
        descriptionID=$('#'+btnID).parents('.panel').find('.panel-body #recdescriptionID_'+btnID+'').html().trim().toLowerCase().replace(/'/g, "\\'").replace(/\&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>') ;
        sendObjectToInspectedPage({action: "code", content: "var cp = document.getElementsByClassName ('listbox')[0];for (var i = 0; i < cp.getElementsByClassName('option').length; i++) {if(cp.getElementsByClassName('option')[i].innerText.trim()=='"+assurecheckpointID+"'){document.getElementsByClassName('option')[i].click();}}if('"+descriptionID+"'!='na'){var selecteddescID=0;var description = document.getElementById ('description_id');for (var i = 0; i < description.options.length; i++) {if (description.options[i].text.trim().toLowerCase()== '"+descriptionID+"'){description.options[i].selected = true;selecteddescID=i;}document.querySelector('#description_id').dispatchEvent(new Event('change', { 'bubbles': true }))};var e = document.getElementById('description_id');var Summary = e.options[selecteddescID].text;document.getElementById('summary').value='"+_gIssueDescriptions+"';document.getElementById('details').focus();}else{var x = document.getElementById('issue');x.querySelectorAll('.own_desc')[0].click();document.getElementById('custom-description').value ='"+_gIssueDescriptions+"';document.getElementById('details').value ='';}"});
        sendObjectToInspectedPage({action: "code", content: "document.getElementById('summary').value='"+_gIssueDescriptions+"';document.getElementById('notes').value ='"+complete_recommendation+"';"});
    if(descriptionID !='na'){
    sendObjectToInspectedPage({action: "code", content: "document.getElementById('custom-description').value ='';var x = document.getElementById('issue');x.querySelectorAll('.library_desc')[0].click()"});
    $("#message").attr('role','alert').addClass('alert alert-success').html("Summary,checkpoint,description,details and recommendation are copied successfully");
    }
    else
    $("#message").attr('role','alert').addClass('alert alert-danger').html("There is no specific description in IDL.Updated with custom summary and description");
}


// widget section. 
//Get JSON file from URL

function bindAllWidgets(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://a11ytest.dequelabs.com/DQLibraries/WL/widgetIndex.json", false);
    xhr.send();
    _WL = JSON.parse(xhr.responseText); 
    bindWidgetNames();
}

// Bind Widget names from JSON data

function bindWidgetNames(){
    var _wdgtids = [],
        _wdgt = [];
    $.each(_WL, function(index, value) {
        if($.inArray(value.WidgetName, _wdgtids) == -1)
        {
            _wdgtids.push(value.WidgetName);
            _wdgt.push(value);
        }
    });
    var ele = document.getElementById('widget');
    for (var i = 0; i < _wdgt.length; i++) {
        // POPULATE SELECT ELEMENT WITH JSON.
        if(ele.innerHTML!=null)
        ele.innerHTML = ele.innerHTML +'<option value='+_wdgt[i].ID+'>' + _wdgt[i].WidgetName + '</option>';
    }
}

// Get widget success criteras applicable to selected widget

$("#widget").on('change',function(e){
    $("#widgetsection").empty(); 
    $("#message").removeAttr().removeClass().empty();
    var scenario = document.getElementById('widget');
    $("#widgetSC").empty();
    getWidgetSCs(scenario.options[scenario.selectedIndex].value);
});

// Get widget issues based on selected widget from JSON file

function getWidgetSCs(widgetID){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://a11ytest.dequelabs.com/DQLibraries/Production/Web_WL/"+widgetID+".json", false);
    xhr.send();
    AllwidgetIssues = JSON.parse(xhr.responseText);
    var _scids = [],
        WidgetSC = [];
    $.each(AllwidgetIssues, function(index, value) {
        if($.inArray(value.CheckpointName, _scids) == -1)
        {
            _scids.push(value.CheckpointName);
            WidgetSC.push(value);
        }
    });
    $('#widgetSC').empty().append('<option value="0">--SELECT--</option>');
    var ele = document.getElementById('widgetSC');
    for (var i = 0; i < WidgetSC.length; i++) {
        // POPULATE SELECT ELEMENT WITH JSON.
        if(ele.innerHTML!=null)
        ele.innerHTML = ele.innerHTML +'<option value='+WidgetSC[i].ID+'>' + WidgetSC[i].CheckpointName + '</option>';
    }
    var _tempWidgetSC=document.getElementById('widgetSC');
    getWidgetIssuesbySC(_tempWidgetSC.options[_tempWidgetSC.selectedIndex].value);
}

// Get widget recommendation based on selected success criteria

function getWidgetIssuesbySC(scID){
    for (var i = 0; i < AllwidgetIssues.length; i++) {
        if(AllwidgetIssues[i].ID==scID){
            WidgetIssueLength.push(i);
            var widgetIssue,
                _scenario=AllwidgetIssues[i].Scenario,
                _ckpID=AllwidgetIssues[i].AssureCkpointID,
                _detailsID=AllwidgetIssues[i].AssureDetailsID,
                _issueDescription=AllwidgetIssues[i].IssueDescription,
                _recommendation=AllwidgetIssues[i].Recommendation;
            widgetIssue="<div id='wdgt_issuesec_"+i+"' class='wdgtpnl panel'>"+
            "<div class='panel-heading panel-blue'>"+
                "<div id='wdgt_issueheading_"+i+"' role='heading' aria-level='4' style='font-size:16px;margin:0px !important'>"+_scenario+"</div>"+
                "<span class='pull-right'><button class='widget btn btn-default btn-sm' id='"+i+"' aria-describedby='wdgt_issueheading_"+i+"'>Copy to Assure</button><button id='copywdgtrec_"+i+"' aria-describedby='wdgt_issueheading_"+i+"' style='margin-left:2px' class='wdgtcopytoclipboard btn btn-default btn-sm'>copy to clipboard</button></span>"+
            "</div>"+
            "<div class='panel-body'>"+
            "<span class='hidden' id='wdgt_issuechkpoint_"+i+"'>"+_ckpID+"</span>"+
            "<span class='hidden' id='wdgt_issuedetailsID_"+i+"'>"+_detailsID+"</span><br/>"+
            "<div id='contenttocopywdgt_"+i+"'>"+
            "<strong>Description: </strong><div id='wdgt_issuecustomdesc_"+i+"'>"+_issueDescription.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\\n/g, '<br/>')+"</div><br/><strong>Recommendation: </strong><div class='wdgtrec' id='wdgt_issuerecom_"+i+"'>"+
            _recommendation.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\\n/g, '<br/>')+
            "</div></div></div></div>";
            $("#widgetsection").append(widgetIssue);
        }
    }
}

// Get recommendation based widget success criteria 

$("#widgetSC").on('change',function(e){
    $("#message").removeAttr().removeClass().empty();
    $("#widgetsection").empty(); 
    var widgetSC = document.getElementById('widgetSC');
    getWidgetIssuesbySC(widgetSC.options[widgetSC.selectedIndex].value);
});

// Attach click event handler to copy to assure button 

$(document).on('DOMNodeInserted','.wdgtpnl', function() { 
    var widgetbns = document.getElementsByClassName("widget"),
        wdgtcopytoclipboard = document.getElementsByClassName("wdgtcopytoclipboard");
    for (i = 0; i < widgetbns.length; i++) {
        widgetbns[i].addEventListener('click',updateWidget, false);
        wdgtcopytoclipboard[i].addEventListener('click',copytoClipboard, false);
        if(i==0){
        var sumallissues=document.getElementById("sumallissues");
        }
}
});

// Send summary, checkpoint description and recommendation data to webpage

function updateWidget(event)
{
    $("#message").removeAttr().removeClass().empty();
    var btnID=event.target.id,
        issuescenario=$('#'+btnID).parents('.panel').find('#wdgt_issueheading_'+btnID+'').html(),
        chkID=$('#'+btnID).parents('.panel').find('.panel-body #wdgt_issuechkpoint_'+btnID+'').html(),
        details=$('#'+btnID).parents('.panel').find('.panel-body #wdgt_issuedetailsID_'+btnID+'').html().trim().toLowerCase(),
        issuedescription=$('#'+btnID).parents('.panel').find('.panel-body #wdgt_issuecustomdesc_'+btnID+'').html().replace(/\<br>/g, '\\n').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/'/g, "\\'"),
        recommendation=$('#'+btnID).parents('.panel').find('.panel-body #wdgt_issuerecom_'+btnID+'').html().replace(/\<br>/g, '\\n').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/'/g, "\\'");
    sendObjectToInspectedPage({action: "code", content: "document.getElementsByName('reopen')[0].setAttribute('checked', 'checked');var cp = document.getElementsByClassName ('listbox')[0];for (var i = 0; i < cp.getElementsByClassName('option').length; i++) {if(cp.getElementsByClassName('option')[i].innerText.trim()=='"+chkID+"'){document.getElementsByClassName('option')[i].click();}}if('"+details+"'!='na'){var selecteddescID=0;var description = document.getElementById('description_id');for (var i = 0; i < description.options.length; i++) {if (description.options[i].text.trim().toLowerCase()== '"+details+"'){description.options[i].selected = true;selecteddescID=i;}};document.querySelector('#description_id').dispatchEvent(new Event('change', { 'bubbles': true }));var e = document.getElementById('description_id');var Summary = e.options[selecteddescID].text;document.getElementById('summary').value=Summary;document.getElementById('details').value ='"+issuedescription+"';}else{var x = document.getElementById('issue');x.querySelectorAll('.own_desc')[0].click();document.getElementById('custom-description').value ='"+issuedescription+"';document.getElementById('details').value ='';document.getElementById('summary').value='"+issuescenario+"'};document.getElementById('notes').value ='"+recommendation+"';"});
    if(details !='na'){
    sendObjectToInspectedPage({action: "code", content: "document.getElementById('custom-description').value ='';var x = document.getElementById('issue');x.querySelectorAll('.library_desc')[0].click()"});
    $("#message").attr('role','alert').addClass('alert alert-success').html("Summary,checkpoint, IDL description, custom details and recommendation are copied successfully");
    }
    else
    $("#message").attr('role','alert').addClass('alert alert-success').html("Summary,checkpoint,custom description and recommendation are copied successfully");
}